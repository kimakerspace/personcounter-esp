#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

#define STASSID "YOUR_WIFI_SSID"
#define STAPSK  "YOUR_WIFI_PASSWORD"
#define API_KEY "YOUR_API_KEY"
#define TRIGGER 12
#define ECHO 14
#define RING 13
#define RING_TOGGLE_PIN 5
#define RING_STATUS_LED 15

volatile bool ring_on = true;

double distance = 120;
const char* ssid = STASSID;
const char* password = STAPSK;
const char* hostname = "Personencounter ESP8266";
const char* host = "home.ki-maker.space";
const uint16_t port = 443;
const String url = "/personcounter_api/inc";
WiFiClientSecure client;


ICACHE_RAM_ATTR void toggle_ring() {
  ring_on = !ring_on;
}


void reconnect_wifi() {
  // if WiFi is down, try reconnecting
  if ((WiFi.status() != WL_CONNECTED)) {
    Serial.print("Reconnecting to WiFi");
    WiFi.disconnect();
    WiFi.reconnect();
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println("Reconnected");
  }
}


double take_measurement() {
  digitalWrite(TRIGGER, LOW);
  delay(5);
  digitalWrite(TRIGGER, HIGH);
  delay(5);
  digitalWrite(TRIGGER, LOW);
  return (0.0343 * (pulseIn(ECHO, HIGH) / 2));
}


double filter(double value, double prev_value, double trust_factor=0.3, double max=120) {
  value =  value * trust_factor + prev_value * (1 - trust_factor);
  return min(value, max);
}


void inc_counter() {
  reconnect_wifi();

  Serial.print("Connecting to ");
  Serial.println(host);

  if (!client.connect(host, port)) {
    Serial.println("Connection failed");
    return;
  }

  Serial.print("Requesting URL: ");
  Serial.println(url);

  client.print(String("PUT ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "User-Agent: PersonCounterESP8266\r\n" +
               "X-API-KEY: " + API_KEY + "\r\n" +
               "Connection: close\r\n\r\n");

  Serial.println("Request sent");
  while (client.connected()) {
    String line = client.readStringUntil('\n');
    Serial.println(line);
    if (line == "\r") {
      Serial.println("Headers received");
      break;
    }
  }
  client.stop();
}


void setup() {
  attachInterrupt(
    digitalPinToInterrupt(RING_TOGGLE_PIN),
    toggle_ring,
    RISING
  );

  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, INPUT);
  pinMode(RING, OUTPUT);
  pinMode(RING_STATUS_LED, OUTPUT);
  pinMode(RING_TOGGLE_PIN, INPUT_PULLUP);

  digitalWrite(RING_STATUS_LED, ring_on);

  Serial.begin(115200);
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.setHostname(hostname);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  client.setInsecure();
}

void loop() {
  digitalWrite(RING_STATUS_LED, ring_on);
  distance = filter(take_measurement(), distance);
  Serial.println(distance);
  delay(10);
  if (distance < 70) {
    if (ring_on) {
      digitalWrite(RING, HIGH);
      delay(500);
      digitalWrite(RING, LOW);
    }
    inc_counter();
    delay(5000);
  }
}
